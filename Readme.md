# Sciences U Presentation

## Objectives

* What's a docker image, a docker container ?
* How to download them ? Launch them ?
* What's docker compose and how to use it ?
* How to build and customize a docker image and how to publish it ?
* How looks like a professional docker compose (nextcloud)

## Commands

## How to manipulate docker images ?

```shell
docker pull nginx # Download the nginx container
docker images # List the downloaded images
docker rmi nginx # Delete the downloaded image
```

### How to manage docker containers ?

```bash
docker run --name web-server nginx # Start the web server
# Web server is started but you can't connect to it
docker rm -f web-server # Stop and remove the web server (needed to be able to construct a new docker with the same name)

docker run --name web-server -p 8000:80 nginx # Start the web server on port 8000
# You can access the default page at http://localhost:8000 in your browser
docker rm -f web-server # Stop and remove the web server

# Start the web server with port mapping and volume in detached mode
docker run --name web-server -p 8000:80 -v $PWD/src:/usr/share/nginx/html:ro -d nginx
# You can access your custom page at http://localhost:8000 in your browser
docker ps # Display currently running containers
docker stats # Display consumption currently 
docker stop web-server # Stop web server containers
docker ps -a # Display all running and stopped containers
docker rm web-server # Stop and remove the web server
```

### How to use docker compose ?

```bash
docker-compose -f webserver.yml up -d # Start the web server
# You can access your custom page at http://localhost:8000 in your browser
docker-compose -f webserver.yml down # Stop the web server
```

### How to build a custom docker image and publish it to DockerHub repository ?

```bash
docker build -t esevellec/demo_sciencesu . # Build your custom image based on the Dockerfile file
docker push esevellec/demo_sciencesu # Push the image to your dockerhub repo (replace esevellec by yours)
```

### How to use our custom image ?

```bash
docker-compose -f webserver_v2.yml up -d # Start the web server
# You can access your custom web server page with your custom container
docker-compose -f webserver_v2.yml down # Stop the web server
```

### What is looking like a real docker compose deployment with multiple containers ?

```bash
docker-compose -f nextcloud.yml up -d # Start the containers
# You can access Nextcloud web page at http://localhost:8000 in your browser
docker-compose -f nextcloud.yml down # Stop the containers
docker-compose -f nextcloud.yml down -v # Stop the containers and delete the datas
```

## Links

* [Nginx dockerhub official image](https://hub.docker.com/_/nginx)
* [NextCloud source docker file from docker repo awesome-compose](https://github.com/docker/awesome-compose/tree/master/nextcloud-redis-mariadb)
